package com.nfyg.timelinerecyclerview;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private RecyclerView rvModel;
    private List<Model> modelList = new ArrayList<>(10);
    private ModelListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        rvModel = (RecyclerView) findViewById(R.id.rvTrace);
        initData();
    }

    private void initData() {
        // 模拟一些假的数据
        modelList.add(new Model("2016-05-25 17:48:00", "[上海市] [上海杨浦区营业点]的派件已签收 感谢使用顺丰速运,期待再次为您服务!"));
        modelList.add(new Model("2016-05-25 14:13:00", "[上海市] [上海杨浦区营业点]的复旦大学代理点正在派件 电话:18040xxxxxx 请保持电话畅通、耐心等待"));
        modelList.add(new Model("2016-05-25 13:01:04", "[上海市] 快件到达 [上海杨浦区营业点]"));
        modelList.add(new Model("2016-05-25 12:19:47", "[上海市] 快件在[上海集散中心]已装车,准备发往[上海杨浦区营业点]"));
        modelList.add(new Model("2016-05-25 11:12:44", "[上海市] 快件到达 [上海集散中心]"));
        modelList.add(new Model("2016-05-24 03:12:12", "[广州市] 快件在[广州总集散中心]已装车,准备发往[上海集散中心]"));
        modelList.add(new Model("2016-05-23 21:06:46", "[广州市] 快件到达 [广州总集散中心]"));
        modelList.add(new Model("2016-05-23 18:59:41", "[深圳市] 快件在[深圳五和集散中心]已装车,准备发往[广州总集散中心]"));
        modelList.add(new Model("2016-05-23 18:35:32", "[深圳市] 顺丰速运已收件 电话:18358xxxxxx"));
        adapter = new ModelListAdapter(this, modelList);
        rvModel.setLayoutManager(new LinearLayoutManager(this));
        rvModel.setAdapter(adapter);
    }
}
