package com.nfyg.timelinerecyclerview;

/**
 * Created by joey.huang on 2017/6/14.
 * 版本：1.0
 * 描述：物流信息类
 * 备注：
 */

public class Model {
    /** 时间 */
    private String acceptTime;
    /** 描述 */
    private String acceptStation;

    public Model() {
    }

    public Model(String acceptStation, String acceptTime) {
        this.acceptStation = acceptStation;
        this.acceptTime = acceptTime;
    }

    public String getAcceptStation() {
        return acceptStation;
    }

    public void setAcceptStation(String acceptStation) {
        this.acceptStation = acceptStation;
    }

    public String getAcceptTime() {
        return acceptTime;
    }

    public void setAcceptTime(String acceptTime) {
        this.acceptTime = acceptTime;
    }
}
